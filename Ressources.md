# CV
* Szeliski, R. (2021). Computer Vision: Algorithms and Applications, [second edition](http://szeliski.org/Book/2ndEdition.htm)

# ML
* Gareth, J., Witten, D., Hastie, T., Tibshirani, R. (2013). An Introduction to Statistical Learning : with Applications in R. New York, Springer

# DL
* Chollet, F. (2017). Deep learning with Python. Manning Publications.
* Géron, A. (2019). Hands-on machine learning with Scikit-Learn, Keras and TensorFlow: concepts, tools, and techniques to build intelligent systems (2nd ed.). O'Reilly.
