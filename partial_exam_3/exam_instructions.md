**Partial Exam 3/3**

1. Choose a publication which uses ML or DL techniques applied to a field of geosciences.
Either select one of the papers in this directory (preferable), or if you have a paper you wish to read that is not in the list, send an email to valade@igeofisica.uam.mx with the paper/title/authors to ask whether it is suited or not for this exam.

2. Read the paper, and prepare a Power Point presention to present it to the lecturers and students. 
The presentation should allow the audience to understand the study's goals (problem to solve), methods applied, and main results. 